'use strict';

const express = require ('express');
const path = require ('path');

//Constantes
const PORT = 8081;

//App
const app = express(); //Inicializamos el servidor

app.use(express.static(__dirname));
//Si lanzamos solo esto, ya estaria escuchando de este puerto
//pero no haría nada
app.get('/', function(req, res){
  //res.send("Bienvenido terrícola....\n");
  res.sendFile(path.join(__dirname+'/index.html'));
}); //cuando metan el localhost:8081 se va a ejecutar esto

app.get('/detallePost/:id', function(req, res){
  //res.send("Bienvenido terrícola....\n");
  res.sendFile(path.join(__dirname+'/detallePost.html'));
}); //cuando metan el localhost:8081 se va a ejecutar esto

app.get('/admin', function(req, res){
  //res.send("Bienvenido terrícola....\n");
  res.sendFile(path.join(__dirname+'/admin/admin.html'));
}); //cuando metan el localhost:8081 se va a ejecutar esto

app.listen(PORT);
console.log("Express funcionando en el puerto " + PORT);
