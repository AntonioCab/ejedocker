var expect = require('chai').expect;
var $ = require ('chai-jquery');
var request = require ('request');

describe ("Pruebas sencillas",function(){
  it('Test suma', function(){
    expect(9+4).to.equal(13);
  });
});

describe ("Pruebas red",function(){

  it('Test internet', function(done){
    request.get("http://www.forocoches.com",
              function(error, response, body){
                expect(response.statusCode).to.equal(200);
                done();
              });
  });

  it('Test local', function(done){
    request.get("http://localhost:8081",
              function(error, response, body){
                expect(response.statusCode).to.equal(200);
                done();
              });
  });

  it('Test body contiene body', function(done){
  request.get("http://localhost:8081",
              function(error, response, body){
                expect(body).contains('terri');
                done();
              });
  });

  it('Test h1', function(done){
    request.get("http://localhost:8081",
              function(error, response, body){
                expect(body).should.exist('<h1>[bB]\w+</h1>');
                done();
              });
  });
});

describe('Test contenido HTML', function (done){
  it ('Test H1', function(done)){
    request.get("http://localhost:8081",
        function(error,response,body){
          expect($("h1")).to.have.text("Bienvenido a mi BLOG");
        });
  });
});   
